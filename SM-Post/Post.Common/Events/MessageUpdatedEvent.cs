﻿using Cqrs.Core.Events;

namespace Post.Common.Events;

public class MessageUpdatedEvent : BaseEvent
{
    public string Message { get; set; } = default!;
    
    public MessageUpdatedEvent() : base(nameof(MessageUpdatedEvent))
    {
    }
}