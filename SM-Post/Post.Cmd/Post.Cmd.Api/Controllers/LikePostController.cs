﻿using Cqrs.Core.Exceptions;
using Cqrs.Core.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Post.Cmd.Api.Commands;
using Post.Common.Dtos;

namespace Post.Cmd.Api.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
public class LikePostController : ControllerBase
{
    private readonly ILogger<LikePostController> _logger;
    private readonly ICommandDispatcher _commandDispatcher;

    public LikePostController(
        ILogger<LikePostController> logger, 
        ICommandDispatcher commandDispatcher)
    {
        _logger = logger;
        _commandDispatcher = commandDispatcher;
    }

    [HttpPut("{id}")]
    public async Task<ActionResult> LikePost(Guid id)
    {
        try
        {
            await _commandDispatcher.Send(new LikePostCommand() { Id = id });

            return Ok(new BaseResponse()
            {
                Message = "Like post request completed successfully"
            });
        }
        catch(InvalidOperationException invalidOperationException)
        {
            _logger.Log(LogLevel.Warning, invalidOperationException, "Client made a bad request");
            return BadRequest(new BaseResponse()
            {
                Message = invalidOperationException.Message
            });
        }
        catch(AggregateNotFoundException aggregateNotFoundException)
        {
            _logger.Log(LogLevel.Warning, aggregateNotFoundException,
                "Could not retrieve aggregate!. Client passed an incorrect post ID targeting the aggregate !");
            return BadRequest(new BaseResponse()
            {
                Message = aggregateNotFoundException.Message
            });
        }
        catch (Exception exception)
        {
            const string SAFE_ERROR_MESSAGE = "Error while processing request to like the message of a post";
            _logger.Log(LogLevel.Error, exception, SAFE_ERROR_MESSAGE);

            return StatusCode(StatusCodes.Status500InternalServerError, new BaseResponse()
            {
                Message = SAFE_ERROR_MESSAGE
            });
        }
    }
}