using Confluent.Kafka;
using Cqrs.Core.Domain;
using Cqrs.Core.Events;
using Cqrs.Core.Handlers;
using Cqrs.Core.Infrastructure;
using Cqrs.Core.Producers;
using MongoDB.Bson.Serialization;
using Post.Cmd.Api.Commands;
using Post.Cmd.Domain.Aggregates;
using Post.Cmd.Infrastructure.Config;
using Post.Cmd.Infrastructure.Dispatchers;
using Post.Cmd.Infrastructure.Handlers;
using Post.Cmd.Infrastructure.Producers;
using Post.Cmd.Infrastructure.Repositories;
using Post.Cmd.Infrastructure.Stores;
using Post.Common.Events;

var builder = WebApplication.CreateBuilder(args);

BsonClassMap.RegisterClassMap<BaseEvent>();
BsonClassMap.RegisterClassMap<PostCreatedEvent>();
BsonClassMap.RegisterClassMap<MessageUpdatedEvent>();
BsonClassMap.RegisterClassMap<PostLikedEvent>();
BsonClassMap.RegisterClassMap<CommentAddedEvent>();
BsonClassMap.RegisterClassMap<CommentUpdatedEvent>();
BsonClassMap.RegisterClassMap<CommentRemovedEvent>();
BsonClassMap.RegisterClassMap<PostRemovedEvent>();


builder.Services.Configure<MongoDbConfig>(builder.Configuration.GetSection(nameof(MongoDbConfig)));
builder.Services.Configure<ProducerConfig>(builder.Configuration.GetSection(nameof(ProducerConfig)));

builder.Services.AddScoped<IEventStoreRepository, EventStoreRepository>();
builder.Services.AddScoped<IEventProducer, EventProducer>();
builder.Services.AddScoped<IEventStore, EventStore>();
builder.Services.AddScoped<IEventSourcingHandler<PostAggregate>, EventSourcingHandler>();
builder.Services.AddScoped<ICommandHandler, CommandHandler>();

var commandDispatcher = RegisterCommandHandlers();
builder.Services.AddSingleton<ICommandDispatcher>(commandDispatcher);

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

CommandDispatcher RegisterCommandHandlers()
{
    //TODO: From my point of view it's better to create scope and get service from it.
    var commandHandler = builder.Services.BuildServiceProvider().GetRequiredService<ICommandHandler>();
    var dispatcher = new CommandDispatcher();
    dispatcher.RegisterHandler<NewPostCommand>(commandHandler.Handle);
    dispatcher.RegisterHandler<EditMessageCommand>(commandHandler.Handle);
    dispatcher.RegisterHandler<LikePostCommand>(commandHandler.Handle);
    dispatcher.RegisterHandler<AddCommentCommand>(commandHandler.Handle);
    dispatcher.RegisterHandler<EditCommentCommand>(commandHandler.Handle);
    dispatcher.RegisterHandler<RemoveCommentCommand>(commandHandler.Handle);
    dispatcher.RegisterHandler<DeletePostCommand>(commandHandler.Handle);
    dispatcher.RegisterHandler<RestoreReadDbCommand>(commandHandler.Handle);


    return dispatcher;
}