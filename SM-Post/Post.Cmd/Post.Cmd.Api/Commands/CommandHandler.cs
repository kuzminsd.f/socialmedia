﻿using Cqrs.Core.Handlers;
using Post.Cmd.Domain.Aggregates;

namespace Post.Cmd.Api.Commands;

public class CommandHandler : ICommandHandler
{
    private readonly IEventSourcingHandler<PostAggregate> _eventSourcingHandler;

    public CommandHandler(IEventSourcingHandler<PostAggregate> eventSourcingHandler)
    {
        _eventSourcingHandler = eventSourcingHandler;
    }

    public async Task Handle(NewPostCommand command)
    {
        var aggregate = new PostAggregate(command.Id, command.Author, command.Message);
        
        await _eventSourcingHandler.Save(aggregate);
    }

    public async Task Handle(EditMessageCommand command)
    {
        var aggregate = await _eventSourcingHandler.GetById(command.Id);
        aggregate.EditMessage(command.Message);
        
        await _eventSourcingHandler.Save(aggregate);
    }

    public async Task Handle(LikePostCommand command)
    {
        var aggregate = await _eventSourcingHandler.GetById(command.Id);
        aggregate.LikePost();
        
        await _eventSourcingHandler.Save(aggregate);
    }

    public async Task Handle(AddCommentCommand command)
    {
        var aggregate = await _eventSourcingHandler.GetById(command.Id);
        aggregate.AddComment(command.Comment, command.Username);
        
        await _eventSourcingHandler.Save(aggregate);
    }

    public async Task Handle(EditCommentCommand command)
    {
        var aggregate = await _eventSourcingHandler.GetById(command.Id);
        aggregate.EditComment(command.CommentId, command.Comment, command.Username);
        
        await _eventSourcingHandler.Save(aggregate);
    }

    public async Task Handle(RemoveCommentCommand command)
    {
        var aggregate = await _eventSourcingHandler.GetById(command.Id);
        aggregate.RemoveComment(command.CommentId, command.Username);
        
        await _eventSourcingHandler.Save(aggregate);
    }

    public async Task Handle(DeletePostCommand command)
    {
        var aggregate = await _eventSourcingHandler.GetById(command.Id);
        aggregate.DeletePost(command.Username);
        
        await _eventSourcingHandler.Save(aggregate);
    }

    public async Task Handle(RestoreReadDbCommand command)
    {
        await _eventSourcingHandler.RepublishEvents();
    }
}