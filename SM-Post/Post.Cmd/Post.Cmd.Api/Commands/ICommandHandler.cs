﻿namespace Post.Cmd.Api.Commands;

public interface ICommandHandler
{
    Task Handle(NewPostCommand command);
    Task Handle(EditMessageCommand command);
    Task Handle(LikePostCommand command);
    Task Handle(AddCommentCommand command);
    Task Handle(EditCommentCommand command);
    Task Handle(RemoveCommentCommand command);
    Task Handle(DeletePostCommand command);
    Task Handle(RestoreReadDbCommand command);

}