﻿using Cqrs.Core.Commands;
using Cqrs.Core.Infrastructure;

namespace Post.Cmd.Infrastructure.Dispatchers;

public class CommandDispatcher : ICommandDispatcher
{
    private readonly Dictionary<Type, Func<BaseCommand, Task>> _handlers = new();

    public void RegisterHandler<T>(Func<T, Task> handler) where T : BaseCommand
    {
        if (_handlers.ContainsKey(typeof(T)))
        {
            //TODO: it's better to use ArgumentException
            throw new IndexOutOfRangeException("You cannot register the same command handler twice!");
        }

        _handlers.Add(typeof(T), x => handler((T)x));
    }

    public async Task Send(BaseCommand command)
    {
        if (_handlers.TryGetValue(command.GetType(), out var handler))
        {
            await handler(command);
        }
        else
        {
            //TODO: it's better to use IndexOutOfRangeException or just ArgumentException
            throw new ArgumentNullException(nameof(handler), "No command handler was registered");
        }
    }
}