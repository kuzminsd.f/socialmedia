﻿using Cqrs.Core.Domain;
using Cqrs.Core.Events;
using Cqrs.Core.Exceptions;
using Cqrs.Core.Infrastructure;
using Cqrs.Core.Producers;
using Post.Cmd.Domain.Aggregates;

namespace Post.Cmd.Infrastructure.Stores;

public class EventStore : IEventStore
{
    private readonly IEventStoreRepository _eventStoreRepository;
    private readonly IEventProducer _eventProducer;

    public EventStore(
        IEventStoreRepository eventStoreRepository, 
        IEventProducer eventProducer)
    {
        _eventStoreRepository = eventStoreRepository;
        _eventProducer = eventProducer;
    }


    public async Task SaveEvents(Guid aggregateId, IEnumerable<BaseEvent> events, int expectedVersion)
    {
        var eventStream = await _eventStoreRepository.FindByAggregateId(aggregateId);

        //TODO: if we aren't gonna change it - need to test. It looks too primitive
        if (expectedVersion != -1 && eventStream[^1].Version != expectedVersion)
        {
            throw new ConcurrencyException();
        }

        var version = expectedVersion;
        foreach (var @event in events)
        {
            version++;
            @event.Version = version;
            var eventType = @event.GetType().Name;

            var eventModel = new EventModel()
            {
                TimeStamp = DateTime.UtcNow,
                AggregateIdentifier = aggregateId,
                AggregateType = nameof(PostAggregate),
                Version = version,
                EventType = eventType,
                EventData = @event
            };

            await _eventStoreRepository.Save(eventModel);

            //TODO: I don't think we need to get it every time
            var topic = Environment.GetEnvironmentVariable("KAFKA_TOPIC")!;
            await _eventProducer.Produce(topic, @event);
        }
    }

    public async Task<List<BaseEvent>> GetEvents(Guid aggregateId)
    {
        var eventStream = await _eventStoreRepository.FindByAggregateId(aggregateId);

        if (eventStream == null || !eventStream.Any())
        {
            throw new AggregateNotFoundException("Incorrect post ID provided!");
        }

        return eventStream.OrderBy(x => x.Version).Select(x => x.EventData).ToList();
    }

    public async Task<List<Guid>> GetAggregateIds()
    {
        var eventStream = await _eventStoreRepository.FindAll();
        
        if (eventStream == null || !eventStream.Any())
        {
            throw new ArgumentNullException(
                nameof(eventStream), 
                "Could not retrieve event stream from the event store!");
        }

        return eventStream
            .Select(x => x.AggregateIdentifier)
            .Distinct()
            .ToList();
    }
}