﻿using Cqrs.Core.Domain;
using Cqrs.Core.Handlers;
using Cqrs.Core.Infrastructure;
using Cqrs.Core.Producers;
using Post.Cmd.Domain.Aggregates;

namespace Post.Cmd.Infrastructure.Handlers;

public class EventSourcingHandler : IEventSourcingHandler<PostAggregate>
{
    private readonly IEventStore _eventStore;
    private readonly IEventProducer _eventProducer;

    public EventSourcingHandler(
        IEventStore eventStore, 
        IEventProducer eventProducer)
    {
        _eventStore = eventStore;
        _eventProducer = eventProducer;
    }

    public async Task Save(AggregateRoot aggregate)
    {
        //TODO: also looks too unreliable in the multithreading paradigm
        // There is possible lost update anomaly
        await _eventStore.SaveEvents(aggregate.Id, aggregate.GetUncommittedChanges(), aggregate.Version);
        aggregate.MarkChangesAsCommitted();
    }

    public async Task<PostAggregate> GetById(Guid aggregateId)
    {
        var aggregate = new PostAggregate();
        var events = await _eventStore.GetEvents(aggregateId);

        if (events == null || !events.Any())
        {
            return aggregate;
        }

        aggregate.ReplayEvents(events);
        var latestVersion = events.Max(x => x.Version);
        aggregate.Version = latestVersion;

        return aggregate;
    }

    public async Task RepublishEvents()
    {
        var aggregateIds = await _eventStore.GetAggregateIds();

        //TODO: extra checking.. If it had been true, an exception would have been thrown before 
        if (aggregateIds == null || !aggregateIds.Any())
        {
            return;
        }

        foreach (var aggregateId in aggregateIds)
        {
            var aggregate = await GetById(aggregateId);

            //TODO: Remove extra checking as well. The first condition is never true
            if (aggregate is null || !aggregate.Active)
            {
                continue;
            }

            var events = await _eventStore.GetEvents(aggregateId);

            foreach (var @event in events)
            {
                //TODO: move it to a static variable of the class
                var topic = Environment.GetEnvironmentVariable("KAFKA_TOPIC");
                await _eventProducer.Produce(topic, @event);
            }
        }

    }
}