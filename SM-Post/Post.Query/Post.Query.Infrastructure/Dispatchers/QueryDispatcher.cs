﻿using Cqrs.Core.Infrastructure;
using Cqrs.Core.Queries;
using Post.Query.Domain.Entities;

namespace Post.Query.Infrastructure.Dispatchers;

public class QueryDispatcher : IQueryDispatcher<PostEntity>
{
    private readonly Dictionary<Type, Func<BaseQuery, Task<List<PostEntity>>>> _handlers = new();

    public void RegisterHandler<TQuery>(Func<TQuery, Task<List<PostEntity>>> handler) where TQuery : BaseQuery
    {
        if (_handlers.ContainsKey(typeof(TQuery)))
        {
            //TODO: it's better to use ArgumentException
            throw new IndexOutOfRangeException("You cannot register the same query handler twice!");
        }
        
        _handlers.Add(typeof(TQuery), x => handler((TQuery)x));
    }

    public async Task<List<PostEntity>> Send(BaseQuery query)
    {
        if (_handlers.TryGetValue(query.GetType(), out var handler))
        {
            return await handler(query);
        }

        //TODO: It's better to use IndexOutOfRangeException
        throw new ArgumentNullException(nameof(handler), "No query handler was registered");
    }
}