﻿using Microsoft.EntityFrameworkCore;
using Post.Query.Domain.Entities;
using Post.Query.Domain.Repositories;
using Post.Query.Infrastructure.DataAccess;

namespace Post.Query.Infrastructure.Repositories;

public class PostRepository : IPostRepository
{
    private readonly DatabaseContextFactory _contextFactory;

    public PostRepository(DatabaseContextFactory contextFactory)
    {
        _contextFactory = contextFactory;
    }

    public async Task Create(PostEntity post)
    {
        using var context = _contextFactory.CreateDbContext();
        context.Posts.Add(post);
        await context.SaveChangesAsync();
    }

    public async Task Update(PostEntity post)
    {
        using var context = _contextFactory.CreateDbContext();
        context.Posts.Update(post);
        await context.SaveChangesAsync();
    }

    public async Task Delete(Guid postId)
    {
        using var context = _contextFactory.CreateDbContext();
        var post = await GetById(postId);

        if (post is null)
        {
            return;
        }

        context.Posts.Remove(post);
        await context.SaveChangesAsync();
    }

    public async Task<PostEntity?> GetById(Guid postId)
    {
        using var context = _contextFactory.CreateDbContext();
        return await context
            .Posts
            .Include(p => p.Comments)
            .FirstOrDefaultAsync(p => p.PostId == postId);
    }

    public async Task<List<PostEntity>> ListAll()
    {
        using var context = _contextFactory.CreateDbContext();
        return await context
            .Posts
            .AsNoTracking()
            .Include(p => p.Comments)
            .AsNoTracking()
            .ToListAsync();
    }

    public async Task<List<PostEntity>> ListByAuthor(string author)
    {
        using var context = _contextFactory.CreateDbContext();
        return await context
            .Posts
            .AsNoTracking()
            .Include(p => p.Comments)
            .AsNoTracking()
            .Where(p => p.Author.Contains(author))
            .ToListAsync();
    }

    public async Task<List<PostEntity>> ListWithLikes(int numberOfLikes)
    {
        using var context = _contextFactory.CreateDbContext();
        return await context
            .Posts
            .AsNoTracking()
            .Include(p => p.Comments)
            .AsNoTracking()
            .Where(p => p.Likes >= numberOfLikes)
            .ToListAsync();
    }

    public async Task<List<PostEntity>> ListWithComments()
    {
        using var context = _contextFactory.CreateDbContext();
        return await context
            .Posts
            .AsNoTracking()
            .Include(p => p.Comments)
            .AsNoTracking()
            .Where(p => p.Comments != null && p.Comments.Any())
            .ToListAsync();
    }
}