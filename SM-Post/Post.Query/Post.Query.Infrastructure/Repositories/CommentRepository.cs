﻿using Microsoft.EntityFrameworkCore;
using Post.Query.Domain.Entities;
using Post.Query.Domain.Repositories;
using Post.Query.Infrastructure.DataAccess;

namespace Post.Query.Infrastructure.Repositories;

public class CommentRepository : ICommentRepository
{
    private readonly DatabaseContextFactory _contextFactory;

    public CommentRepository(DatabaseContextFactory contextFactory)
    {
        _contextFactory = contextFactory;
    }

    public async Task Create(CommentEntity comment)
    {
        using var context = _contextFactory.CreateDbContext();
        context.Comments.Add(comment);
        await context.SaveChangesAsync();
    }

    public async Task Update(CommentEntity comment)
    {
        using var context = _contextFactory.CreateDbContext();
        context.Comments.Update(comment);
        await context.SaveChangesAsync();
    }

    public async Task<CommentEntity?> GetById(Guid commentId)
    {
        using var context = _contextFactory.CreateDbContext();
        return await context.Comments.FirstOrDefaultAsync(c => c.CommentId == commentId);
    }

    public async Task Delete(Guid commentId)
    {
        using var context = _contextFactory.CreateDbContext();
        var comment = await GetById(commentId);

        if (comment is null)
        {
            return;
        }
        
        context.Comments.Remove(comment);
        await context.SaveChangesAsync();
    }
}