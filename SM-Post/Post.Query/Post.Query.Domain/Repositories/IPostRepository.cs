﻿using Post.Query.Domain.Entities;

namespace Post.Query.Domain.Repositories;

public interface IPostRepository
{
    Task Create(PostEntity post);
    Task Update(PostEntity post);
    Task Delete(Guid postId);
    Task<PostEntity?> GetById(Guid postId);
    Task<List<PostEntity>> ListAll();
    Task<List<PostEntity>> ListByAuthor(string author);
    Task<List<PostEntity>> ListWithLikes(int numberOfLikes);
    Task<List<PostEntity>> ListWithComments();
}