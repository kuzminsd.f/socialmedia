﻿using Post.Query.Domain.Entities;

namespace Post.Query.Domain.Repositories;

public interface ICommentRepository
{
    Task Create(CommentEntity comment);
    Task Update(CommentEntity comment);
    Task<CommentEntity?> GetById(Guid commentId);
    Task Delete(Guid commentId);
}