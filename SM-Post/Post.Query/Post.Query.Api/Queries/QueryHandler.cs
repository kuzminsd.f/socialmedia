﻿using Post.Query.Domain.Entities;
using Post.Query.Domain.Repositories;

namespace Post.Query.Api.Queries;

public class QueryHandler : IQueryHandler
{
    private readonly IPostRepository _postRepository;

    public QueryHandler(IPostRepository postRepository)
    {
        _postRepository = postRepository;
    }

    public async Task<List<PostEntity>> Handle(FindAllPostsQuery query)
    {
        return await _postRepository.ListAll();
    }

    public async Task<List<PostEntity>> Handle(FindPostByIdQuery query)
    {
        var post = await _postRepository.GetById(query.Id);
        return new List<PostEntity>() { post };
    }

    public async Task<List<PostEntity>> Handle(FindPostsByAuthorQuery query)
    {
        return await _postRepository.ListByAuthor(query.Author);
    }

    public async Task<List<PostEntity>> Handle(FindPostsWithCommentsQuery query)
    {
        return await _postRepository.ListWithComments();
    }

    public async Task<List<PostEntity>> Handle(FindPostsWithLikesQuery query)
    {
        return await _postRepository.ListWithLikes(query.NumberOfLikes);
    }
}