﻿using Post.Query.Domain.Entities;

namespace Post.Query.Api.Queries;

public interface IQueryHandler
{
    Task<List<PostEntity>> Handle(FindAllPostsQuery query);
    Task<List<PostEntity>> Handle(FindPostByIdQuery query);
    Task<List<PostEntity>> Handle(FindPostsByAuthorQuery query);
    Task<List<PostEntity>> Handle(FindPostsWithCommentsQuery query);
    Task<List<PostEntity>> Handle(FindPostsWithLikesQuery query);
}