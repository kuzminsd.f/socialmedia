using Confluent.Kafka;
using Cqrs.Core.Consumers;
using Cqrs.Core.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Post.Query.Api.Queries;
using Post.Query.Domain.Entities;
using Post.Query.Domain.Repositories;
using Post.Query.Infrastructure.Consumers;
using Post.Query.Infrastructure.DataAccess;
using Post.Query.Infrastructure.Dispatchers;
using Post.Query.Infrastructure.Handlers;
using Post.Query.Infrastructure.Repositories;
using EventHandler = Post.Query.Infrastructure.Handlers.EventHandler;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
Action<DbContextOptionsBuilder> configureDbContext =
    (o => o
        .UseLazyLoadingProxies()
        .UseSqlServer(builder.Configuration.GetConnectionString("SqlServer")));

builder.Services.AddDbContext<DatabaseContext>(configureDbContext);
builder.Services.AddSingleton<DatabaseContextFactory>(new DatabaseContextFactory(configureDbContext));

CreateDatabaseAndTables();

builder.Services.AddScoped<IPostRepository, PostRepository>();
builder.Services.AddScoped<ICommentRepository, CommentRepository>();
builder.Services.AddScoped<IQueryHandler, QueryHandler>();
builder.Services.AddScoped<IEventHandler, EventHandler>();
builder.Services.Configure<ConsumerConfig>(builder.Configuration.GetSection(nameof(ConsumerConfig)));
builder.Services.AddScoped<IEventConsumer, EventConsumer>();
builder.Services.AddHostedService<ConsumerHostedService>();

var queryDispatcher = RegisterQueryHandlers();
builder.Services.AddSingleton<IQueryDispatcher<PostEntity>>(queryDispatcher);

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

void CreateDatabaseAndTables()
{
    var dataContext = builder
        .Services
        .BuildServiceProvider()
        .GetRequiredService<DatabaseContext>();

    dataContext.Database.EnsureCreated();
}

QueryDispatcher RegisterQueryHandlers()
{
    var queryHandler = builder
        .Services
        .BuildServiceProvider()
        .GetRequiredService<IQueryHandler>();

    var dispatcher = new QueryDispatcher();
    dispatcher.RegisterHandler<FindAllPostsQuery>(queryHandler.Handle);
    dispatcher.RegisterHandler<FindPostByIdQuery>(queryHandler.Handle);
    dispatcher.RegisterHandler<FindPostsByAuthorQuery>(queryHandler.Handle);
    dispatcher.RegisterHandler<FindPostsWithCommentsQuery>(queryHandler.Handle);
    dispatcher.RegisterHandler<FindPostsWithLikesQuery>(queryHandler.Handle);

    return dispatcher;
}