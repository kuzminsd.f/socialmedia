﻿using Cqrs.Core.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Post.Common.Dtos;
using Post.Query.Api.Dtos;
using Post.Query.Api.Queries;
using Post.Query.Domain.Entities;

namespace Post.Query.Api.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
public class PostLookupController : ControllerBase
{
    private readonly ILogger<PostLookupController> _logger;
    private readonly IQueryDispatcher<PostEntity> _queryDispatcher;

    public PostLookupController(
        ILogger<PostLookupController> logger, 
        IQueryDispatcher<PostEntity> queryDispatcher)
    {
        _logger = logger;
        _queryDispatcher = queryDispatcher;
    }

    [HttpGet]
    public async Task<ActionResult> GetAllPosts()
    {
        try
        {
            var posts = await _queryDispatcher.Send(new FindAllPostsQuery());

            return NormalResponse(posts);
        }
        catch (Exception ex)
        {
            const string SAFE_ERROR_MESSAGE = "Error while processing request to retrieve all posts!";
            return ErrorResponse(ex, SAFE_ERROR_MESSAGE);
        }
    }
    
    [HttpGet("byId/{postId}")]
    public async Task<ActionResult> GetPostById(Guid postId)
    {
        try
        {
            var posts = await _queryDispatcher.Send(new FindPostByIdQuery()
            {
                Id = postId
            });

            return NormalResponse(posts);
        }
        catch (Exception ex)
        {
            const string SAFE_ERROR_MESSAGE = "Error while processing request to find post by ID!";
            return ErrorResponse(ex, SAFE_ERROR_MESSAGE);
        }
    }
    
    [HttpGet("byAuthor/{author}")]
    public async Task<ActionResult> GetPostsByAuthor(string author)
    {
        try
        {
            var posts = await _queryDispatcher.Send(new FindPostsByAuthorQuery()
            {
                Author = author
            });


            return NormalResponse(posts);
        }
        catch (Exception ex)
        {
            //TODO: add author name to a message..
            const string SAFE_ERROR_MESSAGE = "Error while processing request to find posts by author!";
            return ErrorResponse(ex, SAFE_ERROR_MESSAGE);
        }
    }
    
    [HttpGet("withComments")]
    public async Task<ActionResult> GetPostsWithComments()
    {
        try
        {
            var posts = await _queryDispatcher.Send(new FindPostsWithCommentsQuery());

            return NormalResponse(posts);
        }
        catch (Exception ex)
        {
            const string SAFE_ERROR_MESSAGE = "Error while processing request to find posts with comments!";
            return ErrorResponse(ex, SAFE_ERROR_MESSAGE);
        }
    }

    [HttpGet("withLikes/{numberOfLikes}")]
    public async Task<ActionResult> GetPostsWithLikes(int numberOfLikes)
    {
        try
        {
            var posts = await _queryDispatcher.Send(new FindPostsWithLikesQuery { NumberOfLikes = numberOfLikes});

            return NormalResponse(posts);
        }
        catch (Exception ex)
        {
            //TODO: add a number of likes to a message
            const string SAFE_ERROR_MESSAGE = "Error while processing request to find posts with likes!";
            return ErrorResponse(ex, SAFE_ERROR_MESSAGE);
        }
    }
    
    

    private ActionResult NormalResponse(List<PostEntity> posts)
    {
        if (posts is null || !posts.Any())
        {
            return NoContent();
        }

        var count = posts.Count;

        return Ok(new PostLookupResponse()
        {
            Posts = posts,
            Message = $"Successfully returned {count} post{(count > 1 ? "s" : string.Empty)}!"
        });
    }

    private ActionResult ErrorResponse(Exception ex, string safeErrorMessage)
    {
        _logger.LogError(ex, safeErrorMessage);

        return StatusCode(StatusCodes.Status500InternalServerError, new BaseResponse()
        {
            Message = safeErrorMessage
        });
    }
}