﻿using Cqrs.Core.Events;

namespace Cqrs.Core.Domain;

public abstract class AggregateRoot
{
    private readonly List<BaseEvent> _changes = new();
    
    protected Guid _id;
    
    public Guid Id => _id;

    public int Version { get; set; } = -1;


    public IEnumerable<BaseEvent> GetUncommittedChanges()
    {
        return _changes;
    }

    public void MarkChangesAsCommitted()
    {
        _changes.Clear();
    }

    private void ApplyChange(BaseEvent @event, bool isNew)
    {
        var method = this.GetType().GetMethod("Apply", new[] { @event.GetType() });

        if (method == null)
        {
            //TODO: The exception looks incorrect again :( 
            throw new ArgumentNullException(
                nameof(method),
                $"The apply message wasn't found in the aggregate for {@event.GetType().Name}");
        }

        method.Invoke(this, new object[] { @event });

        if (isNew)
        {
            _changes.Add(@event);
        }
    }

    protected void RaiseEvent(BaseEvent @event)
    {
        ApplyChange(@event, true);
    }

    public void ReplayEvents(IEnumerable<BaseEvent> events)
    {
        foreach (var @event in events)
        {
            ApplyChange(@event, false);
        }
    }
}