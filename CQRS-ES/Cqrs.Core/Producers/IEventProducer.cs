﻿using Cqrs.Core.Events;

namespace Cqrs.Core.Producers;

public interface IEventProducer
{
    Task Produce<T>(string topic, T @event) where T : BaseEvent;
}