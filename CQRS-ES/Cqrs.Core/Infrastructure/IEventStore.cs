﻿using Cqrs.Core.Events;

namespace Cqrs.Core.Infrastructure;

public interface IEventStore
{
    Task SaveEvents(Guid aggregateId, IEnumerable<BaseEvent> events, int expectedVersion);
    Task<List<BaseEvent>> GetEvents(Guid aggregateId);
    Task<List<Guid>> GetAggregateIds();
}