﻿using Cqrs.Core.Domain;

namespace Cqrs.Core.Handlers;

public interface IEventSourcingHandler<T>
{
    Task Save(AggregateRoot aggregate);
    Task<T> GetById(Guid id);
    Task RepublishEvents();
}